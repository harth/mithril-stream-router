## mithril-stream-router

This is a very early draft at an opinionated router. Use with caution.

[Low Level Example](https://flems.io/#0=N4IgZglgNgpgziAXAbVAOwIYFsZJAOgAsAXLKEAGhAGMB7NYmBvEAXwvW10QICsEqdBk2J4hcYgAIASrQCujSQF4AOmkmS4cgA4wATvMZ78xAJ66AFAHJZCmFYqTgajRqsBBKACM5WOFcRJKwB6BxdXD29fAKDgjCi-YMR4nywAfQBrGFMw9QjPVIAxaHtAkJTfOCSK9KzTYO1CWmJaJMhYNIgAE1yNVgBKcLU1cSlbI2VJcKx8CT0YbHwDOz0LZzyNOYWsQJmt7ApwjTADLABVaQAZQLk9KGUAPhlDGHwsDGJqQgB5VckLfqPZ52fC0MD4AqVAGOW73fqHDaSFoXa6SAAeQPGr2RVwsaPhR0kAHMYMQAAofQiBAFAgDuEDQXVotPwUFo1A+EHo+G0lMwOHCAyGaFGkkhfkm01oXRg9yUT3Wrkk82It3UWGs+Bq-kJGgoGqshAAjA4guL-INEXqDXIoFZda4KMgrN80PZHFYACq02imr2Eeb2AC6Do0bww2gsodcGSBBowvSVSaVWOMUAZsejyY0FkkOi6HxgABJJJbs+X-sDGKDweK1pIaplsoFYwNS1MrdnHJnO0qy9n+8nw5H0XHrOnTfjSw7B9O8qxhiL6BIxQlJXl6426pI2-KnEdCaKvBg4DBJuUElUrABqLfZa8hRrNVr2xEqtWSA1ahKv5P66zGn64pBLeCRNqYc5-gahAAEx+sUsAWg6-5WLav5ds67h+gAQn6ADCVghr2kjDrmsZ7lmrjxqalEpi8aYZh2FZJrm+aFiWgK0a4uZYjWEIJAhMCbmBdSOO0MCdF0LY7oCnHEX+kg9sxGhyeWs5KqRo57gaE6OFO6mQa4-YLmgIzLlI4qCeuObAA2InZGJJSSTJQKKq4756OqmriehSooYBHpWVYkjXpI4nOQ+YWnEEIV2ak4GGSpgqLqKABuEAwLS1mftKspAuItCwKytBEhY9KMsyxUcsQXJoEQ8xgI4WC5VAgIAD5tZ+moRtovmSCh2jzKavE4pcFjNTKUBLC8-SzihPjEC0aCmrZ9DUOm1AZNSgJ7uVTIsoQEASLQeimPgx6bTS7BBNhGCbcFc28WAhVdGsDrmoE5oIsm4qfQk31JpZJR-UUJQA30gLjS1012ICFa0eElpmWgK4MhANXxNlNJ7m5Ggeeoqb4GjGNQGsQrzsKopsYwJZKExn6zMQ8zYAClPmTlk20-TexM9sswcmguYWLQFBgDtTxgML8KSMTEDxNCebaAWNNzsjK7pZltNSpzpETXle4a7SUOTbNwpqITEgYHoxDG7KRb9KR1PFkjaCG0WpHhKlvNntpSxMDKqxMtQvgiOd0qmI4XuBnDagu5QICnrA1A1cueBGgALIg6dsBwID8twBDUHAAg0PQjDMDwbBBlQ6ZoBkCAoJwOB4Fg6MBtA8ewngJDENocCIMEwRyGg2gZES+B0FgwSt8Q7dQAAAka+BLwAbNPbd6NA+D8PHZi6HgcDUJv2iiOwTcFzPc8ALT7Fgnd3N3i19wPQ8j2PE+0FPl+b1AN+89gi9l74AAAzBFvtvEue8C6H2PqfXO+cD46H0PRe+5AeA92foPYeo9x6TzAUg5YRh57AKAQAZmCF0I6xB8G6EIfoN4DIIG73MNAo+EAT453Pi3De0A-7MywFfOhxhW51R3lQLu6Cn79ywW-XBn916zx-nw7Ygj6LEKATBQRMBDZXwAJwUKoQo6+t9VErAYaIyBLCD5sI4awIMrAgA)

[High Level Example](https://flems.io/#0=N4Igxg9gdgzhA2BTEAucD4EMAONEBMQAaEAMwEskZUBtUKTAW2TQDoALAF0fmPSk6IBqECAC+RekxYhWAK2olIAoZxHKYnAAQBBeACMAroxhaAvFoA6ULVoAUwLQHNEnAMqdMgolvjkoANY+AEoQhoJaYgCU5gB8WsDWtrYATq6GKTaMdgDkrJgGxjA5Scm2RNk57ACMOT45ekYmOVGlZRW5hvAlNmXJRDQ5APJQiHVaOQAqAO4Q41PsaWMAum19rIw4dmt9WgFxWpWY436Bdlqh4YisEKSsjcYOWgVNAPoBiACeKHuRMVE+fatXq7YG7WwbLZaAAeB0qfnG0P+Oy0YOSaLE1msGm0D0Y5isvSeL2M7y+PlOQQuYQi0QOiRBaxxWn0mDwBJyAHoSSZOTkANQ8smfflc7DsCCcCB8tZpTgZLK5fKFRg9XYdKq1ep4iaClXC1EojXsABM8wAYpREMU0e1Kl01eCBg15gAheYAYRyqxB6022HO+zM8RRuyO41Du0p50ughudzxlqQxP1H0+PgoSFe5HwP320Vt4Pav0jZRikcLZUhAZhcNyCJ8SMNvtshYxWKgzMTVoJpRTbzTGat2fwf3psvSmUOSszYyNlRqFp7OS0-K0s5Hq5XpBSEHxK7XQrTzbLpUxUGx0E0WjSUHwiBSvd6Xk4KVMweneRw2Ed5Uq2DScYXzfVhY2uKUAFVggAGTsYCYFYFx3E8QQ7CiVhdyuKJCw1IxOClKBxkcaAwD8MAAh+NCDmmfx8AgaYOHITQIBST5WFZciqIkCZXUwciVxw+DQJpa5SAQfAHBRPEYEomIP2kuD8LfAEpJVH5MIieSVSeVhdPgnxdIwkS-iIVSmiTRB1OMrTzKtHS9KUmADN0jTEBMtY6UU18EKQjwvEQNCjKwotklKYFrEYDYwgEOw6LAYxVHYiB8HTew5PiBlbGZMDzDWGBDGwB8RJSVhOE+QrcjAoiUQaFVih+Lk6hqvEcga7k6s5FAjy+JqWwmbskFaiZ2qaGBOu6z5OXFSVpRQDcc16spojWUo5QVBI1gAN3IRBplkg4UWySLNDSJggsEErIEYbBoFUHxMvBRhTL62wTsQJgfmO193qeyMdz3KDoJ+DJ4AOMDIU4MB2CGFJzioj9wdue46rQnwQdRZ6i0gmCflhBGRNKiBAbsJFMfBeAIDALxyGgfaPxou96NYCmqc4GmoDJ3Z-HINmCjQh6izWqcBZC1yfkRhMUcrPpzyLCRIzwFJtrAa1+fll6byEe8Uh+eCDlvbX7PyRyQmM5a+vN2xz2iPg8CQMA2avEQAAYUGdgBaAB2T2UBNcRJBABhmBEVgwBgRR+EEYQ0HEZYSEpagUDoQPpBERgecWSg+BBkQuE4XAUE5TlDCgbAAicUO905dPOEz+AAAFqlYJuADZq4zlJKHkCOysKkQYDATvsDUCQpGDtAa7r923qYbOUl4NA84LouS7Liurvb2vO-gafvqYRvm9YZ3ORnyKFD4XuZAHoeR4DoOr4KoqrhSOeF5AJeZJX0vy8rxgT8f1yKR67O0PgAZk5PgJinB-6FUARsfw3cL7lSvoPcgw9-ZjxkJPbeu9TqMHdnA9OUBEEkBzovfCy9i7f3XlXbBlBcE-QIcVYBh8TQEMQNtXa7sACcECoGbynqfJhz94HEPPiQS+-dUHoLEMsMQQA)

# Quick Start

```js
const Albums = 
  ({ getState, link, Route }) => {
    return m('.albums'
      ,m('h1', 'Albums')
      ,m('ul'
        ,['One', 'Two', 'Three']
        .map(
          k => m('a', link( Route.of.Album({ album_key: k }) ), k )
        )
        .map( x => m('li', x) )
      )
    )
  }

const Album = 
  ({ album_key, link, Route }) => {
   
    const base = '/albums/'+album_key+'/photo/'
    return m('.album'
      ,m('h1', 'Album '+album_key )
      ,m('h2', 'Files')
      ,m('ul'
        ,['A', 'B', 'C']
        .map( k => 
            m('a', 
             link( Route.of.AlbumFile({ album_key, file_id: k }))
              , k 
            ) 
        )
        .map( x => m('li', x) )
      )
    )
  }

const AlbumFile = 
  ({ album_key, file_id }) => {
    return m('.file'
      ,m('h1', 'File ' + file_id +' from ' + album_key )
    )
  }

const render = 
  attrs => m('.app'
    ,m('pre', attrs.Route.toURL(attrs.getState().route))
    ,m('button', { onclick: () => window.history.back() }, 'Back' )
    ,attrs.Route.fold({
      Albums: () => Albums(attrs),
      Album: route => Album({ ...attrs, ...route }),
      AlbumFile: route => AlbumFile({ ...attrs, ...route }),
    }) (attrs.getState().route)           
  )

m.mount(document.body, () => {
  const Route =
    superouter.type('Route', {
      'Albums': '/',
      'Album': '/albums/:album_key',
      'AlbumFile': '/albums/:album_key/photo/:file_id',
    })
    
  return {
    view: () => 
      m(m.stream.router.component, {
        m,
        stream: m.stream,
        fromURL: url => Route.matchOr( () => Route.of.Albums(), url ),
        toURL: x => Route.toURL(x),
        location: () => window.location,
        initial(){
          return {
            route: Route.of.Albums()
          }
        },
        services(){},
        render: attrs => render({ ...attrs, Route })
      })
  }
})
```
