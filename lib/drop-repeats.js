const equals = (a, b) => JSON.stringify(a) === JSON.stringify(b)
export default createStream => select => s => {
    const s2 = createStream()
    let hasVal = false
    s.map(
        newVal => {
            if (!hasVal || !equals(select(newVal), select(s2()))) {
                s2(newVal)
            }
            hasVal = true
            return null
        }
    )
    return s2
}