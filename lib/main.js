import router from './index.js'

function Main({
  attrs: {
    m,
    stream,
    services,
    render,
    initial,
    toURL,
    fromURL,
    reducer: theirReducer,
    location
  }
}) {

  const reducer = theirReducer || ((x, f) => f(x))
  const Router = router({
    toURL
    , fromURL
    , getPath: () => location.pathname
    , stream
  })

  const setState = stream()

  const getState = stream.scan(
    reducer
    , initial({ location })
    , setState
  )

  const link = route =>
    Router.link(setState)(route)

  const attrs = {
    link, getState, setState
  }

  return {
    oncreate() {
      Router
        .start( getState )
        .map( setState )

      if( services ) {
        services(attrs)
      }

      getState.map(
        () => m.redraw()
      )
    }
    , view: () => render(attrs)
  }
}


router.component = Main
export default router