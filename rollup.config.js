import babel from 'rollup-plugin-babel'
import {terser} from 'rollup-plugin-terser'

export default {
    input: './lib/main.js'
    ,plugins: [
        babel()
        // ,terser()
    ]
    ,output: {
        file: './dist/mithril-stream-router.min.js'
        ,format: 'umd'
        ,name: 'm.stream.router'
        ,sourcemap: 'external'
    },

}
